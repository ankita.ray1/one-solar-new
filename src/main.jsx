import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import AboutUs from './components/AboutUs.jsx';
import Homes from './components/Homes.jsx';
import GatedCommunities from './components/GatedCommunities.jsx';
import Industrial from './components/Industrial.jsx';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/about-us",
    element: <AboutUs />,
  },
  {
    path: "/homes",
    element: <Homes />,
  },
  {
    path: "/gated-communities",
    element: <GatedCommunities />,
  },
  {
    path: "/industrial",
    element: <Industrial />,
  }
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

