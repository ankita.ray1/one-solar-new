import backgroundImage from "../assets/solar-panels.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import FreeQuoteButton from "./FreeQuoteButton";

function CommercialBanner() {

  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 0.5,
      },
    },
  };
  return (
    <>
      <div
        className="bg-cover bg-center h-screen"
        style={{ backgroundImage: `url(${backgroundImage})` }}
      >
        <motion.div
          className="absolute text-white top-1/2 transform -translate-y-1/2 mx-2 text-center sm:left-1/2  sm:-translate-x-1/2"
          variants={containerVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          ref={ref}
        >
          <motion.h1
            className="text-2xl md:text-3xl lg:text-4xl font-bold whitespace-normal sm:whitespace-nowrap"
            variants={containerVariants}
          >
            Shining a Light on Commercial Success: Your Trusted Solar Solutions
            Partner
          </motion.h1>
          <FreeQuoteButton WhiteTheme={true}/>
        </motion.div>
      </div>
    </>
  );
}

export default CommercialBanner;
