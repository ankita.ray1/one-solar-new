import booking from "../assets/gatedcommunities-booking.png";
import AppointmentForm from "./AppointmentForm";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function GatedCommunityBooking() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  const itemVariants = {
    hidden: {
      opacity: 0,
      x: -50,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  const formVariants = {
    hidden: {
      opacity: 0,
      x: 40,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };
  return (
    <>
      <div className="px-4 md:px-20 lg:px-36 my-16">
        <h1 className="text-lg md:text-xl font-semibold mb-6">
          Benefits up to ₹90 Lakh
        </h1>
        <h1 className="text-lg md:text-xl font-semibold">
          RWA and Group Housing Societies, a subsidy of Rs 18,000 every KW for
          up to 500 KW will be given.
        </h1>
      </div>
      <div
        className="bg-cover min-h-screen flex flex-col md:flex-row gap-16 items-center justify-center p-12 md:p-20 lg:p-36 mb-16"
        style={{ backgroundImage: `url(${booking})` }}
      >
        <motion.div
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
          className="w-full md:w-1/2 text-center"
        >
          <motion.h2
            variants={itemVariants}
            className="text-2xl md:text-3xl lg:text-4xl font-semibold text-white"
          >
            Solar Made Simple with us!
          </motion.h2>
        </motion.div>
        <motion.div
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={formVariants}
          className="w-full md:w-1/2"
        >
          <AppointmentForm />
        </motion.div>
      </div>
    </>
  );
}

export default GatedCommunityBooking;
