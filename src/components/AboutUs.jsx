import Navbar from "./Navbar"
import Testimonials from "./Testimonials"
import Questions from "./Questions"
import Blog from "./Blog"
import Footer from "./Footer"
import Values from "./Values"
import Inclusive from "./Inclusive"
import AboutIntro from "./AboutIntro"
import AboutUsItems from "./AboutUsItems"
import { useRef } from "react"

function AboutUs() {
  const calculatorRef = useRef(null);

  const scrollToQuote = () => {
    calculatorRef.current.scrollIntoView({ behavior: 'smooth' });
  };
  return (
    <div>
      <Navbar blackTheme={true} scrollToQuote={scrollToQuote}/>
      <AboutIntro/>
      <Values/>
      <Inclusive ref={calculatorRef}/>
      <AboutUsItems/>
      <Testimonials/>
      <Questions/>
      <Blog/>
      <Footer/>
    </div>
  )
}

export default AboutUs
