import {
  FaInstagram,
  FaFacebook,
  FaXTwitter,
  FaYoutube,
  FaLinkedin,
  FaLocationDot,
} from "react-icons/fa6";
import { MdOutlineMail } from "react-icons/md";
import { BiPhoneCall } from "react-icons/bi";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useNavigate } from "react-router-dom";

function Footer() {
  var currentDate = new Date();
  var currentYear = currentDate.getFullYear();
  
  const navigate = useNavigate();
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
      },
    },
  };
  const handleClick = () => {
    navigate("/", { behavior: "smooth" });
  };
  return (
    <section className="bg-gray-950 px-4 md:px-20 lg:px-36 pt-12 md:pt-20 lg:pt-36 pb-6 text-white bg-[#070708]">
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
        <motion.div
          variants={containerVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          ref={ref}
          className="flex flex-col items-center lg:items-start justify-center my-6"
        >
          <a href="/" className="text-2xl font-semibold italic">
            One Solar
          </a>
          <p className="my-4">Energize Your Future.</p>
          <div className="flex items-center space-x-8 mt-4 lg:mt-16">
            <FaInstagram size={20} />
            <FaFacebook size={20} />
            <FaXTwitter size={20} />
            <FaYoutube size={20} />
            <FaLinkedin size={20} />
          </div>
        </motion.div>

        <motion.div
          variants={containerVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          ref={ref}
          className="flex flex-col items-center lg:items-start justify-center my-6"
        >
          <h4 className="text-lg font-semibold my-4">Our Solutions</h4>
          <a href="/homes" className="my-1 lg:my-3 font-medium">
            Homes
          </a>
          <a href="/gated-communities" className="my-1 lg:my-3 font-medium">
            Gated Communities
          </a>
          <a href="/commercials" className="my-1 lg:my-3 font-medium">
            Commercial
          </a>
          <a href="/about-us" className="my-1 lg:my-3 font-medium">
            About us
          </a>
          <a href="/" className="my-1 lg:my-3 font-medium">
            Blogs
          </a>
        </motion.div>

        <motion.div
          variants={containerVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          ref={ref}
          className="flex flex-col items-center lg:items-start justify-center my-6"
        >
          <div className="flex gap-4 lg:gap-20 mb-6 lg:mb-20">
            <button
              onClick={handleClick}
              className="bg-black text-white rounded-full px-2 py-1 border border-white text-sm"
            >
              Get Free Quote
            </button>
            <div className="flex gap-4">
              <a href="/">
                <MdOutlineMail size={24} />
              </a>
              <a href="/">
                <BiPhoneCall size={24} />
              </a>
            </div>
          </div>
          <div className="flex flex-col lg:flex-row gap-2 items-center justify-center">
            <FaLocationDot size={30} />
            <p className="text-sm text-center lg:text-start">
              Eshika Building, Plot no: 26-27, Old Mumbai Hwy, Prashant Hills,
              Gachibowli, Rai Durg, Telangana 500096
            </p>
          </div>
        </motion.div>
      </div>
      <motion.p className="text-center text-sm mt-24">
        {`Copyright ©${currentYear}  One Solar | Privacy Policy | All Rights Reserved.`}
      </motion.p>
    </section>
  );
}

export default Footer;
