import Navbar from "./Navbar";
import Blog from "./Blog";
import Booking from "./Booking";
import Footer from "./Footer";
import HomeBanner from "./HomeBanner";
import Process from "./Process";
import Questions from "./Questions";
import Service from "./Service";
import Testimonials from "./Testimonials";
import Calculator from "./Calculator";
import { useRef } from 'react';
import GridPlusIntro from "./GridPlusIntro";
import Features from "./Features";
import SuryaGhar from "./SuryaGhar";

function Home() {
  const calculatorRef = useRef(null);

  const scrollToCalculator = () => {
    calculatorRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <>
      <Navbar blackTheme={true} scrollToCalculator={scrollToCalculator}/>
      <HomeBanner scrollToCalculator={scrollToCalculator}/>
      <GridPlusIntro/>
      <Features/>
      {/* <Service /> */}
      {/* <Booking /> */}
      <Process />
      <SuryaGhar/>
      <Calculator />
      <Booking ref={calculatorRef}/>
      <Questions />
      <Testimonials />
      <Blog />
      <Footer />
    </>
  );
}

export default Home;
