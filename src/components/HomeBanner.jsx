import backgroundImage from "../assets/backgroundImage.jpeg";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import FreeQuoteButton from "./FreeQuoteButton";

// eslint-disable-next-line react/prop-types
const HomeBanner = ({scrollToCalculator}) => {
  
  const [ref, inView] = useInView({
    triggerOnce: false,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 0.5,
      },
    },
  };

  return (
    <div
      className="bg-cover bg-center h-screen relative"
      style={{ backgroundImage: `url(${backgroundImage})` }}
    >
      <motion.div
        className="absolute top-1/2 transform -translate-y-1/2 mx-2 text-center sm:left-1/2  sm:-translate-x-1/2"
        variants={containerVariants}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        ref={ref}
      >
        <motion.h1
          className="text-2xl md:text-3xl lg:text-4xl font-bold whitespace-normal sm:whitespace-nowrap"
          variants={containerVariants}
        >
          Free yourself from rising energy costs
        </motion.h1>
        <motion.p
          className="my-4 text-lg md:text-xl lg:text-2xl font-bold whitespace-normal sm:whitespace-nowrap"
          variants={containerVariants}
        >
          Switch to Solar and Save Big on Your Energy Bills!
        </motion.p>
        <FreeQuoteButton WhiteTheme={false} scrollToCalculator={scrollToCalculator}/>  
      </motion.div>
    </div>
  );
};

export default HomeBanner;
