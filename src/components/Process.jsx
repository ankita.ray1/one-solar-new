import businessMan1 from "../assets/business-man-1.png";
import businessMan2 from "../assets/business-man-2.png";
import Consultation from "../assets/Consultation.png";
import Design from "../assets/Design.png";
import Support from "../assets/Online Support.png";
import Man from "../assets/Popular Man.png";
import Analytics from "../assets/Financial Analytics.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function Process() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const headingVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
      },
    },
  };
  const itemVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };


  return (
    <div className="my-20 px-4 md:px-20 lg:px-36 relative">
      <motion.h1
        className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start"
        variants={headingVariants}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
      >
        End-to-end hassle-free process
      </motion.h1>
      <motion.div
        variants={itemVariants}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        ref={ref}
        className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-5 gap-6 pt-16 text-center"
      >
        <div className="flex flex-col items-center justify-center gap-4">
          <img src={Consultation} alt="consultation" className="w-14 h-14"/>
          <h2 className="text-lg font-medium">1. Free Consultation</h2>
          <p className="text-md font">
          Personalized advice to meet your energy goals, ensuring efficiency and savings.
          </p>
        </div>
        <div className="flex flex-col items-center justify-center gap-4">
          <img src={Design} alt="Design" className="w-14 h-14"/>
          <h2 className="text-lg font-medium">2. Clean Design</h2>
          <p className="text-md font">
          Tech and expertise customize solutions for your property while cutting costs.
          </p>
        </div>
        <div className="flex flex-col items-center justify-center gap-4">
          <img src={Analytics} alt="Analytics" className="w-14 h-14"/>
          <h2 className="text-lg font-medium">3. Easy Financing & Process</h2>
          <p className="text-md font">
          Easy access to renewable energy. No paperwork hassle, flexible funding options.
          </p>
        </div>
        <div className="flex flex-col items-center justify-center gap-4">
          <img src={Man} alt="man" className="w-14 h-14"/>
          <h2 className="text-lg font-medium">4. Expert Installation</h2>
          <p className="text-md font">
          Trust our certified installers for seamless setup, meeting industry standards.
          </p>
        </div>
        <div className="flex flex-col items-center justify-center gap-4">
          <img src={Support} alt="Support" className="w-14 h-14"/>
          <h2 className="text-lg font-medium">5. 24/7 Support</h2>
          <p className="text-md font">
          We're here for you 24/7, from setup to support, ensuring your peace of mind.
          </p>
        </div>
        <div className="flex items-center justify-center mt-4 md:hidden">
          <img src={businessMan1} alt="Business Man 1" />
        </div>
      </motion.div>
      {/* <motion.img
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={imgVariants}
        src={businessMan1}
        className="hidden xl:block absolute bottom-16 right-36 w-56"
        alt="Business Man 1"
      />
      <motion.img
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={imgVariants}
        src={businessMan2}
        className="hidden xl:block absolute bottom-0 right-80 w-56"
        alt="Business Man 2"
      /> */}
    </div>
  );
}

export default Process;
