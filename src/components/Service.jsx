import { useState, useEffect } from "react";
import homes from "../assets/homes.png";
import commercials from "../assets/commercials.png";
import FiveItems from "./FiveItems";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function Service() {
  const [animationPlayed, setAnimationPlayed] = useState(false);
  const [ref, inView] = useInView({
    triggerOnce: true, 
    rootMargin: "-50px 0px",
  });

  useEffect(() => {
    if (inView && !animationPlayed) {
      setAnimationPlayed(true);
    }
  }, [inView, animationPlayed]);

  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  const itemVariants = {
    hidden: {
      opacity: 0,
      y: 20,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  return (
    <div className="px-4 md:px-20 lg:px-36 my-16">
      <motion.div
        ref={ref}
        initial="hidden"
        animate={animationPlayed ? "visible" : "hidden"}
        variants={containerVariants}
      >
        <h1 className="font-inter text-center font-semibold text-2xl md:text-3xl lg:text-4xl mb-16">
          Reliable Solar Solutions: Trusted Solutions for Affordable,
          Sustainable, and Hassle-Free Renewable Energy
        </h1>
        <FiveItems />
        <motion.div
          className="flex flex-col sm:flex-row justify-center items-center gap-y-0 sm:gap-x-3 md:gap-x-20 text-xl text-center font-semibold"
          variants={containerVariants}
        >
          <motion.div variants={itemVariants} className="w-full sm:w-1/2 xl:w-1/4 p-5 sm:p-0 flex flex-col items-center justify-center">
            <img src={homes} alt="Homes" className="mx-auto mb-4 transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105" />
            <h2 className="mb-4">Individual</h2>
          </motion.div>
          <motion.div variants={itemVariants} className="w-full sm:w-1/2 xl:w-1/4 p-5 sm:p-0 flex flex-col items-center justify-center">
            <img src={commercials} alt="Commercials" className="mx-auto mb-4 transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105" />
            <h2 className="mb-4">Industrial</h2>
          </motion.div>
        </motion.div>
      </motion.div>
    </div>
  );
}

export default Service;
