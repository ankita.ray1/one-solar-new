import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { SlArrowLeftCircle, SlArrowRightCircle } from "react-icons/sl";
import { FaStar } from "react-icons/fa";
import { useInView } from "react-intersection-observer";

function Testimonials() {
  const testimonials = [
    {
      name: "John Doe",
      testimonial:
        "I couldn't be happier with the solar installation from Solar Solutions. From start to finish, the team was professional, knowledgeable, and efficient. Now I'm saving money on my electricity bills and feeling great about reducing my carbon footprint. Highly recommend!",
      location: "New York",
    },
    {
      name: "Jane Smith",
      testimonial:
        "Choosing Solar Solutions for our home's solar installation was one of the best decisions we've made. The process was seamless, and the installation team was courteous and skilled. Our energy bills have significantly decreased, and we're thrilled with the environmental impact of going solar.",
      location: "Los Angeles",
    },
    {
      name: "Michael Johnson",
      testimonial:
        "I was impressed by the expertise and attention to detail displayed by the Solar Solutions team during our solar installation. They took the time to explain everything and ensure we were comfortable with the system. The results speak for themselves – we're enjoying clean, renewable energy and lower electricity bills.",
      location: "Chicago",
    },
  ];

  const [currentIndex, setCurrentIndex] = useState(0);
  const [numDots, setNumDots] = useState(testimonials.length);
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIndex((prevIndex) => (prevIndex + 1) % testimonials.length);
    }, 5000);
    setNumDots(testimonials.length);
    return () => clearInterval(interval);
  }, [testimonials.length]);

  const containerVariants = {
    hidden: {
      opacity: 0,
      x: 10,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 1.5,
        ease: "easeOut",
      },
    },
  };

  return (
    <div className="mb-20 px-4 md:px-20 lg:px-36">
      <h1 className="font-inter text-center font-semibold text-2xl md:text-3xl lg:text-4xl my-16">
        &quot;Happy Clients, Happy Us&quot;
      </h1>
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={containerVariants}
      >
        <div className="relative">
          <div className="hidden md:block absolute top-1/2 left-2 lg:left-10 transform -translate-y-1/2">
            <button
              onClick={() =>
                setCurrentIndex(
                  (currentIndex - 1 + testimonials.length) % testimonials.length
                )
              }
              className="text-gray-300"
            >
              <SlArrowLeftCircle size={40} />
            </button>
          </div>
          <div className="hidden md:block absolute top-1/2 right-2 lg:right-10 transform -translate-y-1/2">
            <button
              onClick={() =>
                setCurrentIndex((currentIndex + 1) % testimonials.length)
              }
              className="text-gray-300"
            >
              <SlArrowRightCircle size={40} />
            </button>
          </div>
          <div className="max-w-lg lg:max-w-xl mx-auto bg-white shadow-md rounded-lg overflow-hidden p-10">
            <motion.div
              key={currentIndex}
              initial={{ x: "50%" }}
              animate={{ x: 0 }}
              transition={{ duration: 0.5 }}
              className="px-4 py-2"
            >
              <div className="text-center my-4">
                <p className="text-lg">{testimonials[currentIndex].testimonial}</p>
              </div>
              <div className="mt-4 flex justify-center">
                {[...Array(5)].map((_, index) => (
                  <FaStar key={index} size={20} />
                ))}
              </div>
              <div className="text-center my-4">
                <p className="text-lg">{testimonials[currentIndex].name}, {testimonials[currentIndex].location}</p>
              </div>
            </motion.div>
          </div>
          <div className="flex justify-center space-x-2 mt-4">
            {[...Array(numDots)].map((_, index) => (
              <div
                key={index}
                className={`w-2 h-2 rounded-full ${
                  index === currentIndex % numDots ? 'bg-gray-500' : 'bg-gray-300'
                }`}
              ></div>
            ))}
          </div>
        </div>
      </motion.div>
    </div>
  );
}

export default Testimonials;

