import light from "../assets/light.png";
import planet from "../assets/planet.png";
import reputation from "../assets/reputation.png";
import honesty from "../assets/honesty.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function Values() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };


  return (
    <>
      <div className="px-4 md:px-20 lg:px-36">
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={containerVariants}
      >
        <h1 className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center xl:text-start my-16">Our Values</h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 xl:grid-cols-4 gap-4">
          <div className="h-72 shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6">
            <h2 className="text-md font-medium">Innovation</h2>
            <img src={light} alt="light" />
            <p className="text-sm">
              We are committed to pushing the boundaries of technology and
              innovation to deliver cutting-edge renewable energy solutions that
              meet the evolving needs of our customers
            </p>
          </div>
          <div className="h-72 shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6">
            <h2 className="text-md font-medium">Sustainability</h2>
            <img src={planet} alt="planet" />
            <p className="text-sm">
            We prioritize sustainability in our operations, products, and partnerships, striving to minimize our carbon footprint and protect the planet for future generations.
            </p>
          </div>
          <div className="h-72 shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6">
            <h2 className="text-md font-medium">Customer Satisfaction</h2>
            <img src={reputation} alt="reputation" />
            <p className="text-sm">
            Customers are at the heart of our business. We are dedicated to delivering exceptional service, tailored solutions, and ongoing support to ensure our customers&apos; utmost satisfaction.
            </p>
          </div>
          <div className="h-72 shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6">
            <h2 className="text-md font-medium">Integrity</h2>
            <img src={honesty} alt="honesty" />
            <p className="text-sm">
              We are committed to pushing the boundaries of technology and
              innovation to deliver cutting-edge renewable energy solutions that
              meet the evolving needs of our customers
            </p>
          </div>
        </div>
        </motion.div>
      </div>
    </>
  );
}

export default Values;
