import AppointmentForm from "./AppointmentForm";
import inspirational from "../assets/inspirational.png";
import checked from "../assets/checked.png";
import customer from "../assets/customer-service.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import React from "react";

const Inclusive = React.forwardRef((props, refr) => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  const itemVariants = {
    hidden: {
      opacity: 0,
      x: -10,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  const formVariants = {
    hidden: {
      opacity: 0,
      x: 10,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  return (
    <>
      <div className="flex flex-col my-20 px-4 md:px-20 lg:px-36">
        <div className="flex flex-col lg:flex-row gap-10 items-center">
          <motion.div
            ref={ref}
            initial="hidden"
            animate={inView ? "visible" : "hidden"}
            variants={containerVariants}
            className="w-full md:w-1/2"
          >
            <motion.h2
              variants={itemVariants}
              className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start mb-10"
            >
              We&apos;ve Got You Covered
            </motion.h2>
            <motion.div variants={itemVariants} className="flex gap-4 my-4">
              <img
                src={inspirational}
                alt="inspirational"
                className="w-12 h-12"
              />
              <h3 className=" text-lg font-medium ">
                Consultation, design, permitting, installation, and monitoring
                are handled by experts.
              </h3>
            </motion.div>
            <motion.div variants={itemVariants} className="flex gap-4 my-4">
              <img src={checked} alt="checked" className="w-12 h-12" />
              <h3 className=" text-lg font-medium ">
                Seamless and hassle-free installation process managed by the
                company.
              </h3>
            </motion.div>
            <motion.div variants={itemVariants} className="flex gap-4 my-4">
              <img src={customer} alt="customer" className="w-12 h-12" />
              <h3 className=" text-lg font-medium">
                Continuous support and monitoring post-installation for optimal
                performance
              </h3>
            </motion.div>
          </motion.div>
          <motion.div
            initial="hidden"
            animate={inView ? "visible" : "hidden"}
            variants={formVariants}
            className="w-full lg:w-1/2 flex justify-end"
          >
            <AppointmentForm />
          </motion.div>
        </div>
        
      </div>
    </>
  );
});

Inclusive.displayName = "Inclusive";


export default Inclusive;
