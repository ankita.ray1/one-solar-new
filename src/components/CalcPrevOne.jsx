import React from "react";
import { useState, useEffect } from "react";
import { Slider } from "@material-tailwind/react";

const Calculator = React.forwardRef((props, ref) => {
  const [calculationType, setCalculationType] = useState("System Size (kW)");

  const [systemType, setSystemType] = useState("Home");
  const [bill, setBill] = useState(2500);
  const [systemSize, setSystemSize] = useState(1);

  const [spaceRequired, setSpaceRequired] = useState(0);
  const [annualEnergy, setAnnualEnergy] = useState(0);
  const [annualSavings, setAnnualSavings] = useState(0);
  const [priceExcludingSubsidy, setPriceExcludingSubsidy] = useState(0);
  const [subsidy, setSubsidy] = useState(0);

  useEffect(() => {
    if (calculationType === "Monthly Electricity Bill") {
      // calculate for system size
      const calculatedSpaceRequired = parseInt(systemSize) * 80;
      let calculatedAnnualEnergy = parseInt(systemSize) * 1440;
      let calculatedAnnualSavings = parseInt(systemSize) * 10080;
      const calculatedPriceExcludingSubsidy = parseInt(systemSize) * 96589;
      const calculatedSubsidy = parseInt(systemSize) * 30000;

      if (systemType === "Home") {
        calculatedAnnualEnergy = parseInt(systemSize) * 1440;
        calculatedAnnualSavings = parseInt(systemSize) * 10080;
      } else if (systemType === "commercial") {
        calculatedAnnualEnergy = parseInt(systemSize) * 120;
        calculatedAnnualSavings = parseInt(systemSize) * 12960;
      }

      setSpaceRequired(calculatedSpaceRequired);
      setAnnualEnergy(calculatedAnnualEnergy);
      setAnnualSavings(calculatedAnnualSavings);
      setPriceExcludingSubsidy(calculatedPriceExcludingSubsidy);
      setSubsidy(calculatedSubsidy);
    } else if (calculationType === "System Size (kW)") {
      // calculate for electricity bill
    }
  }, [calculationType, systemSize, bill, systemType]);

  const change = () => {
    setCalculationType((prevType) =>
      prevType === "Monthly Electricity Bill"
        ? "System Size (kW)"
        : "Monthly Electricity Bill"
    );
  };
  return (
    <div
      className="flex flex-col lg:flex-row  gap-32 justify-center items-center w-full px-4 md:px-20 lg:px-36 py-12 mb-16  scroll-mt-32"
      ref={ref}
    >
      <div className="flex flex-col gap-4">
        <h2 className="text-2xl font-semibold mb-2">Calculate Your Savings</h2>
        <p className="text-sm mb-4">
          Explore the Potential of Solar Energy and Start Saving From Day 1!
        </p>
        <div className="flex items-center justify-center gap-4">
          <div>
            {" "}
            <label className="font-semibold">Do You Need Solar For</label>
            <select
              value={systemType}
              onChange={(e) => setSystemType(e.target.value)}
              className="w-full px-3 py-2 border border-gray-300 rounded-md"
            >
              <option value="Home">Home</option>
              <option value="commercial">Commercial</option>
            </select>
          </div>

          <div>
            <label className="font-semibold">Select you state</label>

            <select
              // value={systemType}
              // onChange={(e) => setSystemType(e.target.value)}
              className="w-full px-3 py-2 border border-gray-300 rounded-md"
            >
              <option value="telengana">Telengana</option>
              <option value="andhra">Andhra Pradesh</option>
            </select>
          </div>
        </div>

        <div className="">
          {calculationType === "Monthly Electricity Bill" ? (
            <>
              <label>Enter system size (kW)</label>
              <div className="w-96 my-4">
                <Slider
                  // value={value}
                  // onChange={handleChange}
                  min={1000}
                  max={10000}
                  step={100}
                  valueLabelDisplay="auto"
                  defaultValue={6000}
                />
                {/* <div>Selected value: {value}</div> */}
              </div>

              {/* <input
                type="number"
                value={systemSize}
                onChange={(e) => setSystemSize(e.target.value)}
                placeholder="Enter system size (kW)"
                className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              /> */}
            </>
          ) : (
            <>
              <label>Enter monthly electric bill (in ₹)</label>
              <div className="w-96 my-4">
                <Slider defaultValue={50} />
              </div>
              {/* <input
                type="number"
                value={bill}
                onChange={(e) => setBill(e.target.value)}
                placeholder="Enter monthly electric bill (in ₹)"
                className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              /> */}
            </>
          )}
          <p className="text-sm mt-1">
            Or calculate savings using{" "}
            <button onClick={change} className="text-blue-800 underline">
              {calculationType}
            </button>
          </p>
        </div>
      </div>
      {/* ///////// */}
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <label htmlFor="systemSize" className="block mb-2">
            System Size
          </label>
          <input
            type="text"
            id="systemSize"
            value={`${systemSize} kW`}
            placeholder="System Size"
            className="w-full px-6 py-5 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            readOnly
          />
        </div>
        <div className="">
          <label htmlFor="spaceRequired" className="block mb-2">
            Space Required
          </label>
          <input
            type="text"
            id="spaceRequired"
            value={`${spaceRequired} sqft`}
            placeholder="Space Required"
            className="w-full px-6 py-5 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            readOnly
          />
        </div>
        <div className="">
          <label htmlFor="annualEnergy" className="block mb-2">
            Annual Energy Generated
          </label>
          <input
            type="text"
            id="annualEnergy"
            value={`${annualEnergy} Units`}
            placeholder="Annual Energy Generated"
            className="w-full px-6 py-5 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            readOnly
          />
        </div>
        <div className="">
          <label htmlFor="annualSavings" className="block mb-2">
            Annual Savings
          </label>
          <input
            type="text"
            id="annualSavings"
            value={`₹ ${annualSavings}`}
            placeholder="Annual Savings"
            className="w-full px-6 py-5 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            readOnly
          />
        </div>
        <div className="">
          {systemType === "Home" ? (
            <label htmlFor="priceExcludingSubsidy" className="block mb-2">
              Price (Excluding Subsidy)
            </label>
          ) : (
            <label htmlFor="priceExcludingSubsidy" className="block mb-2">
              Price
            </label>
          )}

          <input
            type="text"
            id="priceExcludingSubsidy"
            value={`₹ ${priceExcludingSubsidy}`}
            placeholder="Price (Excluding Subsidy)"
            className="w-full px-6 py-5 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
            readOnly
          />
        </div>
        <div className="">
          {systemType === "Home" && (
            <>
              <label htmlFor="subsidy" className="block mb-2">
                Subsidy
              </label>
              <input
                type="text"
                id="subsidy"
                value={`₹ ${subsidy}`}
                placeholder="Subsidy"
                className="w-full px-6 py-5 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                readOnly
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
});

Calculator.displayName = "Calculator";

export default Calculator;
