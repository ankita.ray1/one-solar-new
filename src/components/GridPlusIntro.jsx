import homes from "../assets/homes.png";
import commercials from "../assets/commercials.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

const GridPlusIntro = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });
  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  const itemVariants = {
    hidden: {
      opacity: 0,
      x: -50,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  const formVariants = {
    hidden: {
      opacity: 0,
      x: 40,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };
  return (
    <>
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={containerVariants}
        className="mt-20 mb-44 px-4 md:px-20 lg:px-36 relative flex flex-col md:flex-row"
      >
        <motion.div
          variants={itemVariants}
          className="w-full md:w-1/2 xl:w-3/4"
        >
          <h1 className="font-inter font-semibold text-xl md:text-3xl lg:text-4xl">
            What is Grid Plus?
          </h1>
          <p className="text-lg sm:text-xl md:text-2xl font-medium pt-16">
            We provide efficient solar power solutions for individuals and
            industries, specializing in sustainable energy systems with
            comprehensive support and cutting-edge technology.
          </p>
        </motion.div>
        <motion.div
          variants={formVariants}
          className="w-full md:w-1/2 xl:w-1/4 relative mt-10 mb-16 flex items-center justify-center"
        >
          <div className=" w-40 sm:w-56 md:w-64 flex gap-4">
            <img src={homes} alt="Homes" className="" />
            <h3 className="mt-14 font-semibold">Individual</h3>
          </div>
          <div className="w-40 sm:w-56 md:w-64 flex items-end gap-4 absolute top-1/2 left-1/3 md:left-1/4 -translate-x-1/4">
            <h3 className="mb-14 font-semibold">Individual</h3>
            <img src={commercials} alt="Commercials" className="" />
          </div>
        </motion.div>
      </motion.div>
    </>
  );
};

export default GridPlusIntro;
