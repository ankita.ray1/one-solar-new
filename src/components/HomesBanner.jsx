import backgroundImage from "../assets/homes-banner.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useState, useEffect } from "react";

function HomesBanner() {
  const [isGlowing, setIsGlowing] = useState(false);

  useEffect(() => {
    const interval = setInterval(() => {
      setIsGlowing((prevGlowing) => !prevGlowing);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 0.5,
      },
    },
  };
  return (
    <>
      <div
        className="bg-cover bg-center h-screen"
        style={{ backgroundImage: `url(${backgroundImage})` }}
      >
        <motion.div
          className="absolute top-1/2 transform -translate-y-1/2 mx-2 text-center sm:left-1/2  sm:-translate-x-1/2"
          variants={containerVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          ref={ref}
        >
          <motion.h1
            className="text-2xl md:text-3xl lg:text-4xl font-bold whitespace-normal sm:whitespace-nowrap"
            variants={containerVariants}
          >
            Power Your Home with Clean, Affordable Solar Energy Today!
          </motion.h1>
          <div className="relative">
            <motion.button
              className={`bg-gradient-to-r from-black to-gray-700 text-white mt-10 px-4 py-2 rounded-full font-medium focus:outline-none ${
                isGlowing ? "ring-4 ring-white ring-opacity-50" : ""
              } transition duration-1000`}
            >
              Get Free Quote
            </motion.button>
          </div>
        </motion.div>
      </div>
    </>
  );
}

export default HomesBanner;
