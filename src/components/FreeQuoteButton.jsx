import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import PropTypes from "prop-types";

// eslint-disable-next-line react/prop-types
const FreeQuoteButton = ({ WhiteTheme,scrollToCalculator }) => {
  const [isGlowing, setIsGlowing] = useState(false);

  useEffect(() => {
    const interval = setInterval(() => {
      setIsGlowing((prevGlowing) => !prevGlowing);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <div className="relative">
        <motion.button
          className={` ${
            WhiteTheme
              ? "bg-gradient-to-r from-white to-gray-200 text-black"
              : "bg-gradient-to-r from-black to-gray-700 text-white"
          }  mt-10 px-4 py-2 rounded-full font-medium focus:outline-none ${
            isGlowing ? "ring-4 ring-white ring-opacity-50" : ""
          } transition duration-1000`}
          onClick={scrollToCalculator}
        >
          Get Free Quote
        </motion.button>
      </div>
    </>
  );
};
FreeQuoteButton.propTypes = {
  WhiteTheme: PropTypes.bool,
};

FreeQuoteButton.defaultProps = {
  WhiteTheme: false,
};

export default FreeQuoteButton;
