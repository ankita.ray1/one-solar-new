import results from "../assets/results.png";
import deliveryTruck from "../assets/delivery-truck.png";
import bestPrice from "../assets/best-price.png";
import battery from "../assets/battery.png";
import savingMoney from "../assets/saving-money.png";


function FiveItems() {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-5 gap-4 my-10">
      
      <div className="shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6 transition duration-300 ease-in-out transform hover:shadow-xl hover:-translate-y-1 hover:scale-105">
        <h2 className="text-md">Honest Pricing</h2>
        <div>
          <img
            className="w-full h-full object-cover"
            src={results}
            alt="Honest Pricing"
          />
        </div>
        <p className="text-sm">
          Transparent quotations with no hidden fees or costs.
        </p>
      </div>
      <div className="shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6 transition duration-300 ease-in-out transform hover:shadow-xl hover:-translate-y-1 hover:scale-105">
        <h2 className="text-md">Easy Financing Options</h2>
        <div>
          <img src={savingMoney} alt="saving money" />
        </div>
        <p className="text-sm">
          Flexible financing to suit every budget, including zero-cost EMI and
          comprehensive insurance plans.
        </p>
      </div>
      <div className="shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6 transition duration-300 ease-in-out transform hover:shadow-xl hover:-translate-y-1 hover:scale-105">
        <h2 className="text-md">Government Subsidy</h2>
        <div>
          <img src={battery} alt="battery" />
        </div>
        <p className="text-sm">
          Subsidies from the government making solar adoption even more
          affordable.
        </p>
      </div>
      <div className="shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6 transition duration-300 ease-in-out transform hover:shadow-xl hover:-translate-y-1 hover:scale-105">
        <h2 className="text-md">Quick Payback</h2>
        <div>
          <img src={bestPrice} alt="bestPrice" />
        </div>
        <p className="text-sm">
          A typical payback period of 4-5 years with significant savings
          thereafter.
        </p>
      </div>
      <div className="shadow-lg rounded-2xl flex flex-col text-center items-center justify-center gap-4 p-6 transition duration-300 ease-in-out transform hover:shadow-xl hover:-translate-y-1 hover:scale-105">
        <h2 className="text-md">Service & Support</h2>
        <div>
          <img src={deliveryTruck} alt="deliveryTruck" />
        </div>
        <p className="text-sm">
          24x7 support from consultation through installation and beyond,
          ensuring lifetime service and maintenance.
        </p>
      </div>
    </div>
  );
}

export default FiveItems;
