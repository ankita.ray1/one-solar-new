import React from "react";
import booking from "../assets/booking.png";
import AppointmentForm from "./AppointmentForm";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

const Booking = React.forwardRef((props, refr) => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  const itemVariants = {
    hidden: {
      opacity: 0,
      x: -50,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  const formVariants = {
    hidden: {
      opacity: 0,
      x: 40, 
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  return (
    <>
      <div
      ref={refr}
        className="bg-cover min-h-screen flex flex-col md:flex-row gap-16 items-center justify-center p-12 md:p-20 lg:p-36"
        style={{ backgroundImage: `url(${booking})` }}
      >
        <motion.div
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
          className="w-full md:w-1/2 text-center"
        >
          <motion.h2
            variants={itemVariants}
            className="text-2xl md:text-3xl lg:text-4xl font-semibold text-white"
          >
            Join the Solar Revolution: Sustainable Energy Starts Here!
          </motion.h2>
        </motion.div>
        <motion.div
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={formVariants}
          className="w-full md:w-1/2"
        >
          <AppointmentForm />
        </motion.div>
      </div>
    </>
  );
});
Booking.displayName = "Booking";


export default Booking;
