import { useState } from "react";
import homebanner from "../assets/homes-banner.png";
import solarHome from "../assets/solarhome.png";
import solarpanels from "../assets/solarpanels.png";
import energy from "../assets/energy.png";
import solar from "../assets/solar-panels2.png";
import { SlArrowLeftCircle, SlArrowRightCircle } from "react-icons/sl";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

const SuryaGhar = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const array = [
    {
      imgurl: homebanner,
      alt: "Home",
      heading1: "Empowering Homes:",
      subheading: "PM-SURYA GHAR: MUFT BIJLI YOJANA Free Electricity Scheme",
      para1:
        "Muft Bijli Yojana launched by PM Modi and how it can benefit households",
      heading2: "What is SURYA GHAR?",
      para2:
        "PM-Surya Ghar: Muft Bijli Yojana is a central scheme that aims to help provide free electricity to households nationwide that opt to install rooftop solar, ensuring access to clean and sustainable energy.",
    },
    {
      imgurl: solarHome,
      alt: "solarHome",
      heading1: "THE PRIMARY PURPOSE",
      subheading: "",
      para1:
        "The Surya Ghar initiative aims to help provide free electricity to households in India who opt to install rooftop solar.",
      heading2: "",
      para2:
        "Switching to solar energy will have a positive environmental impact and provide access to affordable and reliable electricity.",
    },
    {
      imgurl: solarpanels,
      alt: "solarHome",
      heading1: "BENEFITS OF THIS SCHEME",
      subheading: "",
      para1:
        "Under this scheme, eligible households will be able to generate 300 units of electricity free every month, reducing electricity bills and leading to long-term cost savings.",
      heading2: "",
      para2:
        "The use of solar power contributes to environmental sustainability by reducing carbon emissions and reliance on fossil fuels.",
      para3:
        "Access to electricity enhances the quality of life by enabling households to get access to activities such as education and healthcare.",
    },
    {
      imgurl: energy,
      alt: "Energy",
      heading1: "ADDITIONAL BENEFITS",
      subheading: "",
      para1:
        "Cost Savings: By switching to solar power, beneficiaries can save substantially on household electricity bills, thereby improving their financial stability.",
      heading2: "",
      para2:
        "Solar power systems require minimal maintenance reducing the financial burden on households over time and, Government subsidies make solar power systems more affordable for households.",
      para3: "",
    },
    {
      imgurl: solar,
      alt: "Solar",
      heading1: "COST EFFECTIVE",
      subheading: "",
      para1:
        "The scheme provides a subsidy of 60% of the benchmark system cost up to 2 kW capacity and an additional 40% of the system benchmark cost for 2 to 3kW capacity",
      para2: "The subsidy has been capped at 3kW capacity.",
      para3:
        "At current benchmark prices, this will mean ₹30,000 subsidy for 1kW system, ₹60,000 for 2kW systems, and ₹78,000 for 3kW systems or higher.",
    },
  ];
  const handlePrev = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === 0 ? array.length - 1 : prevIndex - 1
    );
  };

  const handleNext = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === array.length - 1 ? 0 : prevIndex + 1
    );
  };
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const itemVariants = {
    hidden: {
      opacity: 0,
      scale: 0.9,
    },
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        duration: 0.5,
        delay: 0.1,
        ease: "easeOut",
      },
    },
  };

  return (
    <div>
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={itemVariants}
        className="mx-4 md:mx-20 lg:mx-32 p-4 md:p-12 my-16 flex flex-col items-center justify-between gap-16 bg-gray-100 rounded-3xl"
      >
        <div className="overflow-hidden w-full">
          <div
            className="flex transition-transform duration-500"
            style={{ transform: `translateX(-${currentIndex * 100}%)` }}
          >
            {array.map((item, index) => (
              <div
                key={index}
                className="flex-shrink-0 w-full flex flex-col lg:flex-row items-center justify-center gap-10"
              >
                <div className="w-full lg:w-1/3">
                  <img
                    src={item.imgurl}
                    alt={item.alt}
                    className="rounded-xl w-full h-auto"
                  />
                </div>
                <div className="w-full lg:w-2/3 font-inter p-4">
                  <h1 className="text-xl mb-4 font-bold">{item.heading1}</h1>
                  <h1 className="text-xl mb-4 font-bold">{item.subheading}</h1>
                  <p className="mb-4">{item.para1}</p>
                  <h1 className="text-xl mb-4 font-bold">{item.heading2}</h1>
                  <p className="mb-4">{item.para2}</p>
                  <p className="mb-4">{item.para3}</p>
                </div>
              </div>
            ))}
          </div>
        </div>

        <div className="flex items-center justify-center gap-2">
          <button onClick={handlePrev} className="text-gray-300">
            <SlArrowLeftCircle size={30} />
          </button>
          <p>
            {currentIndex + 1}/{array.length}
          </p>
          <button onClick={handleNext} className="text-gray-300">
            <SlArrowRightCircle size={30} />
          </button>
        </div>
      </motion.div>
    </div>
  );
};

export default SuryaGhar;
