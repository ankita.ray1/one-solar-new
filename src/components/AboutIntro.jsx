import aboutUsIntro from "../assets/about-us-intro.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function AboutIntro() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });
  const imgVariants = {
    hidden: {
      opacity: 0,
      scale: 0.9,
    },
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        duration: 0.5,
        delay: 0.1,
        ease: "easeOut",
      },
    },
  };
  return (
    <>
      <div className="px-4 md:px-20 lg:px-36 mt-36 flex flex-col gap-4">
        <div className="flex flex-col lg:flex-row">
          <div className="w-full lg:w-1/2">
            <h3 className="text-lg font-semibold text-center lg:text-start">
              ABOUT US
            </h3>
            <h2 className="mt-4 font-semibold text-xl md:text-2xl lg:text-3xl text-center lg:text-start">
              Shaping Sustainable Futures:
            </h2>
            <h2 className="mb-16 font-semibold text-xl md:text-2xl lg:text-3xl text-center lg:text-start">
              Pioneering Renewable Energy Solutions
            </h2>

            <p className="text-xl font-poppins text-center lg:text-start">
              Our mission is to empower individuals, communities, and businesses
              to embrace renewable energy solutions for a sustainable future. We
              strive to make clean energy accessible, affordable, and reliable.
            </p>
          </div>
          <div className="hidden lg:block lg:w-1/2">
            <motion.img
              initial="hidden"
              animate={inView ? "visible" : "hidden"}
              variants={imgVariants}
              src={aboutUsIntro}
              className="p-8"
              alt="About Us Intro"
            />
          </div>
        </div>
        <div className="flex items-center justify-center lg:hidden">
          <img src={aboutUsIntro} alt="About Us Intro" className="opacity-90" />
        </div>
        <p ref={ref} className="text-xl text-center lg:text-start">
          To lead the transition towards a cleaner, greener world where
          renewable energy is the primary power source.
        </p>
      </div>
    </>
  );
}

export default AboutIntro;
