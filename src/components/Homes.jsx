import Blog from "./Blog";
import Footer from "./Footer";
import HomeBooking from "./HomeBooking";
import HomeServices from "./HomeServices";
import HomesBanner from "./HomesBanner";
import Navbar from "./Navbar";
import Process from "./Process";
import ProtectInvestment from "./ProtectInvestment";
import Questions from "./Questions";
import Testimonials from "./Testimonials";

function Homes() {
  return (
    <>
      <Navbar blackTheme={true}/>
      <HomesBanner />
      <HomeServices />
      <Process/>
      <HomeBooking/>
      <ProtectInvestment/>
      <Questions/>
      <Testimonials/>
      <Blog/>
      <Footer/>
    </>
  );
}

export default Homes;
