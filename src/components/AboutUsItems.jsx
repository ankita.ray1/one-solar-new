import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import light from "../assets/light-coin.png";
import home from "../assets/home-coin.png";
import footprint from "../assets/footprint.png";

const AboutUsItems = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const bottomVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  return (
    <div className="flex flex-col my-20 px-4 md:px-20 lg:px-36">
      {/* Significant savings */}
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={bottomVariants}
        className="flex flex-col md:flex-row gap-6 items-center md:items-start justify-center md:justify-start my-6"
      >
        <img src={light} alt="light-coin" className="w-52" />
        <div className="text-center md:text-start">
          <h3 className="font-semibold text-xl my-3">Significant Savings</h3>
          <p className="text-lg">
            Our solar solutions provide significant savings on electricity
            bills, offering a cost-effective alternative to traditional energy
            sources.
          </p>
        </div>
      </motion.div>

      {/* Property value and efficiency */}
      <motion.div
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={bottomVariants}
        className="flex flex-col md:flex-row gap-6 items-center md:items-start justify-center md:justify-start my-6"
      >
        <img src={home} alt="home" className="w-52" />
        <div className="text-center md:text-start">
          <h3 className="font-semibold text-xl my-3">
            Property value and efficiency
          </h3>
          <p className="text-lg">
            Installing solar panels can improve property value while increasing
            energy efficiency, providing long-term benefits for homeowners and
            businesses alike.
          </p>
        </div>
      </motion.div>

      {/* Reduced Environmental Impact */}
      <motion.div
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={bottomVariants}
        className="flex flex-col md:flex-row gap-6 items-center md:items-start justify-center md:justify-start my-6"
      >
        <img src={footprint} alt="Footprint" className="w-52" />
        <div className="text-center md:text-start">
          <h3 className="font-semibold text-xl my-3">
            Reduced Environmental Impact
          </h3>
          <p className="text-lg">
            By promoting solar power, we contribute to reducing carbon
            footprints and mitigating environmental impact, combating climate
            change.
          </p>
        </div>
      </motion.div>
    </div>
  );
};

export default AboutUsItems;
