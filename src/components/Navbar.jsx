import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Hamburger from "hamburger-react";
import Drawer from "react-modern-drawer";
import "react-modern-drawer/dist/index.css";
import { AiFillCloseSquare } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import logo from "../assets/solarPanel.png"

// eslint-disable-next-line react/prop-types
function Navbar({ blackTheme,scrollToCalculator}) {

  const navigate = useNavigate();
  const navigation = [
    { name: "Individual", href: "/homes" },
    { name: "Industrial", href: "/industrial" },
    { name: "About us", href: "/about-us" },
    { name: "More", href: "/" },
  ];
  const [isScrolled, setIsScrolled] = useState(false);
  const [isOpen, setOpen] = useState(false);

  const handleScroll = () => {
    setIsScrolled(window.scrollY > 0);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  const handleClick = () => {
    navigate("/", { behavior: "smooth" });
  };
  return (
    <>
      <div
        className={`fixed top-0 z-50 w-full h-16 md:h-20 px-4 lg:px-20 flex items-center justify-between font-medium ${
          isScrolled
            ? blackTheme
              ? "bg-white bg-opacity-100 shadow-md transform ease-in duration-300 translate-y-0"
              : " bg-[#070708] bg-opacity-100 shadow-md transform ease-in duration-300 translate-y-0"
            : "-translate-y-2.5"
        }`}
      >
        <a
          href="/"
          className={`text-xl md:text-2xl lg:text-2xl font-bold italic ${
            blackTheme ? "text-black" : "text-white"
          }`}
        >
          <div className="flex items-center justify-center gap-1">
          <img src={logo} alt="logo" className="text-md" size={20}/>
          <h2>GRID PLUS</h2>
          </div>
         
        </a>
        <div className="lg:hidden">
          <Hamburger
            toggled={isOpen}
            toggle={setOpen}
            color={blackTheme ? "#000000" : "#ffffff"}
            size={20}
          />

          <Drawer open={isOpen} onClose={() => setOpen(false)} direction="top">
            <div className="flex flex-col items-center mt-6 text-md ">
              <button
                onClick={() => setOpen(false)}
                className="absolute top-4 right-4"
              >
                <AiFillCloseSquare size={30} />
              </button>
              {navigation.map((item) => (
                <a key={item.name} href={item.href} className="my-1">
                  {item.name}
                </a>
              ))}
              <button className="my-1">Contact Us</button>
              <button className="rounded-full px-2 py-1 bg-[#070708] text-white mt-1">
                Get Free Quote
              </button>
            </div>
          </Drawer>
        </div>
        <div className="hidden text-md lg:flex items-center justify-center gap-8 font-semibold">
          {navigation.map((item) => (
            <a
              key={item.name}
              href={item.href}
              className={blackTheme ? "text-black" : "text-white"}
            >
              {item.name}
            </a>
          ))}
        </div>

        <div className="text-md hidden lg:flex items-center justify-center gap-5">
          <button
            className={blackTheme ? "text-black" : "text-white"}
            onClick={handleClick}
          >
            Contact Us
          </button>
          <button
            className={`rounded-full px-2 py-1 ${
              blackTheme ? "bg-black text-white" : "bg-white text-black"
            }`}
            onClick={scrollToCalculator}
          >
            Get Free Quote
          </button>
        </div>
      </div>
    </>
  );
}

Navbar.propTypes = {
  blackTheme: PropTypes.bool,
};

Navbar.defaultProps = {
  blackTheme: false,
};

export default Navbar;
