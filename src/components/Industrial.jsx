import CommercialBanner from "./CommercialBanner";
import Navbar from "./Navbar";
import SolarSolution from "./SolarSolution";
import Process from "./Process";
import Blog from "./Blog";
import Footer from "./Footer";
import CommercialBooking from "./CommercialBooking";

function Industrial() {
  return (
    <>
      <Navbar blackTheme={false}/>
      <CommercialBanner />
      <SolarSolution/>
      <CommercialBooking/>
      <Process/>
      <Blog/>
      <Footer/>
    </>
  );
}

export default Industrial;
