import solarPannel2 from "../assets/solar-panel-2.png";
import AppointmentForm from "./AppointmentForm";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function CommercialBooking() {
    const [ref, inView] = useInView({
        triggerOnce: true,
        rootMargin: "-50px 0px",
      });
    
      const containerVariants = {
        hidden: {
          opacity: 0,
          y: 50,
        },
        visible: {
          opacity: 1,
          y: 0,
          transition: {
            duration: 1,
            ease: "easeOut",
          },
        },
      };
    
      const itemVariants = {
        hidden: {
          opacity: 0,
          x: -50,
        },
        visible: {
          opacity: 1,
          x: 0,
          transition: {
            duration: 0.5,
            delay: 0.5,
            ease: "easeOut",
          },
        },
      };
      const formVariants = {
        hidden: {
          opacity: 0,
          x: 50,
        },
        visible: {
          opacity: 1,
          x: 0,
          transition: {
            duration: 0.5,
            delay: 0.5,
            ease: "easeOut",
          },
        },
      };
  return (
    <>
      <div className="hidden xl:block relative mt-80">
        <img
          src={solarPannel2}
          alt="solar Pannel 2"
          className="w-full h-auto"
        />
        <motion.div
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
          className="flex items-center justify-center gap-60 mx-36 mt-16 absolute -top-1/2"
        >
          <motion.h1
            variants={itemVariants}
            className="text-2xl font-semibold w-1/2 mb-60"
          >
            Elevate Your Business with Sustainable Solar Solutions
          </motion.h1>
          <motion.div
            initial="hidden"
            animate={inView ? "visible" : "hidden"}
            variants={formVariants}
            className="w-1/2"
          >
            <AppointmentForm />
          </motion.div>
        </motion.div>
      </div>

      {/* //for smaller screeen */}

      {/* <div
        className="bg-cover min-h-screen flex flex-col md:flex-row gap-16 items-center justify-center p-12 md:p-20 lg:p-36 xl:hidden"
        style={{ backgroundImage: `url(${solarPannel2})` }}
      >
        <motion.div
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
          className="w-full md:w-1/2 text-center"
        >
          <motion.h2
            variants={itemVariants}
            className="text-2xl md:text-3xl lg:text-4xl font-semibold text-white"
          >
            Join the Solar Revolution: Sustainable Energy Starts Here!
          </motion.h2>
        </motion.div>
        <motion.div
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={formVariants}
          className="w-full md:w-1/2"
        >
          <AppointmentForm />
        </motion.div>
      </div> */}
    </>
  )
}

export default CommercialBooking
