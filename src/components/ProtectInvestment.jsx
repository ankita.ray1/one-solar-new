import investment from "../assets/investment.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function ProtectInvestment() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });
  const headingVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 0.5,
      },
    },
  };
  const containerVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };
  const itemVariants = {
    hidden: {
      opacity: 0,
      x: -30,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };

  const imgVariants = {
    hidden: {
      opacity: 0,
      x: 10,
    },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: 0.2,
        ease: "easeOut",
      },
    },
  };
  return (
    <>
      <div className="px-4 md:px-20 lg:px-36 mb-20">
        <motion.h1
          className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start"
          variants={headingVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
        >
          Protect Your Investment with us
        </motion.h1>

        <div className="flex flex-col md:flex-row my-16 gap-6 text-center md:text-start">
          <motion.div
            ref={ref}
            initial="hidden"
            animate={inView ? "visible" : "hidden"}
            variants={containerVariants}
            className="w-full lg:w-4/5"
          >
            <motion.div variants={itemVariants}>
              <h2 className="font-semibold text-lg">Long-Term Performance</h2>
              <p className="my-4">
                We offer long-term warranties on solar panel systems to ensure
                performance and reliability, providing peace of mind to our
                customers.
              </p>
            </motion.div>
            <motion.div variants={itemVariants}>
              <h2 className="font-semibold text-lg">
                Equipment and Installation
              </h2>
              <p className="my-4">
                Generous warranty terms cover both equipment and installations,
                demonstrating our commitment to quality and customer
                satisfaction.
              </p>
            </motion.div>
            <motion.div variants={itemVariants}>
              <h2 className="font-semibold text-lg">Customer Support</h2>
              <p className="my-4">
                Throughout the warranty period, we remain dedicated to customer
                satisfaction, offering support and assistance as needed to
                address any concerns or issues.
              </p>
            </motion.div>
          </motion.div>
          <motion.div
            initial="hidden"
            animate={inView ? "visible" : "hidden"}
            variants={imgVariants}
            className="py-4"
          >
            <img
              src={investment}
              alt="Investment"
              className="transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105"
            />
          </motion.div>
        </div>
      </div>
    </>
  );
}

export default ProtectInvestment;
