import estimate from "../assets/Estimate.png";
import Bribery from "../assets/Bribery.png";
import Parliament from "../assets/Parliament.png";
import TimetoPay from "../assets/TimetoPay.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

const Features = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });
  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  return (
    <>
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={containerVariants}
        className="mx-4 md:mx-20 lg:mx-32 px-12 py-16 mt-16 flex flex-col lg:flex-row items-center justify-between gap-16 bg-gray-100 rounded-3xl"
      >
        <div className="font-inter font-semibold text-xl md:text-3xl lg:hidden">
          <h1>
            Reliable Solar Solutions: Trusted Solutions for Affordable,
            Sustainable, and Hassle-Free Renewable Energy
          </h1>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 lg:gap-16 w-full lg:w-1/2">
          <div className="flex flex-col items-center justify-center text-center lg:items-start lg:justify-start lg:text-start">
            <img src={estimate} alt="estimate" className="w-10 mb-2" />
            <h2 className="text-lg font-medium">Honest Pricing</h2>
            <p className="text-md font">
              Transparent quotations with no hidden fees or costs.
            </p>
          </div>
          <div className="flex flex-col items-center justify-center text-center lg:items-start lg:justify-start lg:text-start">
            <img src={Bribery} alt="estimate" className="w-10 mb-2" />
            <h2 className="text-lg font-medium">Easy Financing Options</h2>
            <p className="text-md font">
              No-cost EMI and inclusive insurance plans, to fit any budget.
            </p>
          </div>
          <div className="flex flex-col items-center justify-center text-center lg:items-start lg:justify-start lg:text-start">
            <img src={Parliament} alt="estimate" className="w-10 mb-2" />
            <h2 className="text-lg font-medium">Government Subsidy</h2>
            <p className="text-md font">
              The Indian government is making solar energy more affordable.
            </p>
          </div>
          <div className="flex flex-col items-center justify-center text-center lg:items-start lg:justify-start lg:text-start">
            <img src={TimetoPay} alt="estimate" className="w-10 mb-2" />
            <h2 className="text-lg font-medium">Quick Payback</h2>
            <p className="text-md font">
              A typical period of 4-5 years with significant savings thereafter.
            </p>
          </div>
        </div>
        <div className="hidden lg:block w-1/2 font-inter font-semibold text-2xl md:text-3xl">
          <h1>
            Reliable Solar Solutions: Trusted Solutions for Affordable,
            Sustainable, and Hassle-Free Renewable Energy
          </h1>
        </div>
      </motion.div>
    </>
  );
};

export default Features;
