import commercialsBuilding from "../assets/commercials-building.png";
import industries from "../assets/industries.png";
import helthSector from "../assets/health-sector.png";
import fuelStation from "../assets/fuel-station.png";
import schools from "../assets/schools.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function SolarSolution() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  return (
    <>
      <div className="my-20 px-4 md:px-20 lg:px-36">
        <motion.div
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
        >
          <h1 className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start mb-16">
            Solar Solutions: Empowering Every Business Sector
          </h1>
          <motion.div
            variants={containerVariants}
            className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5"
          >
            <div className="flex flex-col items-center justify-center">
              <img
                src={commercialsBuilding}
                alt="Honest Pricing"
                className="w-20"
              />
              <p className="text-lg font-semibold mt-6">Commercial buildings</p>
            </div>
            <div className="flex flex-col items-center justify-center">
              <img src={industries} alt="Honest Pricing" className="w-20" />
              <p className="text-lg font-semibold mt-6">Industries</p>
            </div>

            <div className="flex flex-col items-center justify-center">
              <img src={helthSector} alt="Honest Pricing" className="w-20" />
              <p className="text-lg font-semibold mt-6">Health Care</p>
            </div>
            <div className="flex flex-col items-center justify-center">
              <img src={fuelStation} alt="Honest Pricing" className="w-20" />
              <p className="text-lg font-semibold mt-6">Fuel Stations</p>
            </div>
            <div className="flex flex-col items-center justify-center">
              <img src={schools} alt="Honest Pricing" className="w-20" />
              <p className="text-lg font-semibold mt-6">
                Educational Institutes
              </p>
            </div>
          </motion.div>
        </motion.div>
      </div>
    </>
  );
}

export default SolarSolution;
