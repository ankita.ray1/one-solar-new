import blog1 from "../assets/blog_1.png";
import blog2 from "../assets/blog_2.png";
import blog3 from "../assets/blog_3.png";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

const Blog = () => {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });

  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  return (
    <div className="px-4 md:px-20 lg:px-36 mb-20">
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={containerVariants}
      >
      <h1 className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start">
        Blogs
      </h1>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
      <motion.div
          className="flex flex-col items-center mt-16"
          variants={containerVariants}
        >
          <a href="https://www.business-standard.com/elections/lok-sabha-election/zero-electricity-bill-surplus-power-pm-modi-s-goals-for-viksit-bharat-124042900228_1.html" target="_blank">
          <p>Apr 29 2024</p>
          <img src={blog1} alt="Blog 1" className="mt-2 mb-4 transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105"/>
          <p className="my-2 font-semibold">
          Every household&apos;s power bill should be zero and sell surplus electricity and earn money
          </p>
          <p>News18</p>
          </a>
          
        </motion.div>
        <motion.div
          className="flex flex-col items-center mt-16"
          variants={containerVariants}
        >
          <a href="https://economictimes.indiatimes.com/wealth/save/how-to-apply-for-pm-surya-ghar-muft-bijli-yojana-a-step-by-step-guide/articleshow/108108826.cms" target="_blank">
          <p>Mar 09, 2024</p>
          <img src={blog2} alt="Blog 2" className="mt-2 mb-4  transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105"/>
          <p className="my-2 font-semibold">
          PM Surya Ghar Muft Bijili Yojana New Rooftop Solar Subsidy and how to apply
          </p>
          <p>Economictimes</p>
          </a>
          </motion.div>

          <motion.div
          className="flex flex-col items-center mt-16"
          variants={containerVariants}
        >
          <a href="https://www.business-standard.com/india-news/pm-modi-announces-pm-surya-ghar-muft-bijli-yojana-to-invest-over-75000-cr-124021500774_1.html" target="_blank">
          <p>Feb 15 2024</p>
          <img src={blog3} alt="Blog 3" className="mt-2 mb-4 transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105"/>
          <p className="my-2 font-semibold">
          Cabinet approves Rs 75k cr rooftop solar scheme, 1 cr households to get subsidy of up to Rs 78k
          </p>
          <p>Businessstandard</p>
          </a>
          </motion.div>
      </div>

      <div className="flex items-center justify-center text-md font-semibold my-6">
        <button>Read more</button>
      </div>
      </motion.div>
    </div>
  );
};

export default Blog;
