import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import {
  Accordion,
  AccordionHeader,
  AccordionBody,
} from "@material-tailwind/react";
import React from "react";

function Questions() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });
  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 100,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };
  // eslint-disable-next-line react/prop-types
  function Icon({ id, open }) {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        strokeWidth={3}
        stroke="currentColor"
        className={`${
          id === open ? "rotate-180" : ""
        } h-5 w-5 transition-transform`}
      >
        {id === open ? (
          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 12H4.5" />
        ) : (
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M12 4.5v15m7.5-7.5h-15"
          />
        )}
      </svg>
    );
  }
  const [open, setOpen] = React.useState(0);

  const handleOpen = (id) => {
    setOpen(open === id ? null : id);
  };

  const faqList = [
    {
      id: 1,
      question: "What is a solar rooftop system?",
      answer:
        "A solar rooftop system is a setup where solar panels are installed on the roof of a building to harness sunlight and convert it into electricity.",
    },
    {
      id: 2,
      question: "How do solar rooftop systems work?",
      answer:
        "Solar panels contain photovoltaic cells that capture sunlight and convert it into direct current (DC) electricity. This electricity is then converted into usable alternating current (AC) electricity by an inverter and utilized to power appliances and devices in the building.",
    },
    {
      id: 3,
      question: "What are the benefits of installing a solar rooftop system?",
      answer:
        "Benefits include reduced electricity bills, lower carbon footprint, energy independence, potential for earning through net metering or feed-in tariffs, and increased property value.",
    },
    {
      id: 4,
      question: "How long do solar rooftop systems last?",
      answer:
        "Solar panels typically come with warranties ranging from 20 to 25 years, but they can last much longer with proper maintenance. Inverters may need replacement every 10 to 15 years.",
    },
    {
      id: 5,
      question:
        "What factors determine the efficiency of a solar rooftop system?",
      answer:
        "Efficiency depends on factors like the quality and orientation of solar panels, local climate and sunlight conditions, shading, roof angle, and system maintenance.",
    },
    {
      id: 6,
      question:
        "How much space do I need on my roof for a solar rooftop system?",
      answer:
        "The required space depends on factors such as the size and efficiency of the solar panels, energy consumption, and local sunlight conditions. A professional installer can assess and provide recommendations based on your specific situation.",
    },
    {
      id: 7,
      question:
        "Are there any government incentives or rebates for installing solar rooftop systems?",
      answer:
        "Many governments offer incentives such as tax credits, rebates, grants, and net metering programs to encourage the adoption of solar energy. These incentives vary by location.",
    },
    {
      id: 8,
      question:
        "Can I still use electricity from the grid with a solar rooftop system?",
      answer:
        "Yes, solar rooftop systems are often connected to the grid, allowing you to draw electricity from the grid when solar production is insufficient, and to feed excess electricity back into the grid.",
    },
    {
      id: 9,
      question: "What maintenance is required for a solar rooftop system?",
      answer:
        "Regular maintenance includes cleaning the panels to remove dirt and debris, checking for shading, monitoring system performance, and inspecting for any damage. Professional maintenance may be recommended periodically.",
    },
    {
      id: 10,
      question: "Is my roof suitable for a solar rooftop system?",
      answer:
        "Factors such as roof orientation, angle, material, age, and structural integrity determine suitability. A professional assessment will determine if your roof is suitable and what modifications may be needed for installation.",
    },
  ];
  const [showAll, setShowAll] = React.useState(false);
  const [text, setText] = React.useState("Read More");
  const handleClick = () => {
    setShowAll(!showAll);
    setText(!showAll ? "Read Less" : "Read More");
  };

  return (
    <>
      <div className="px-4 md:px-20 lg:px-36 my-20">
        <motion.div
          className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start mb-6"
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
        >
          <h1 className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start mb-6">
            Frequently Asked Questions
          </h1>
          {!showAll ? (
            <>
              {faqList.slice(0, 6).map((item) => (
                <Accordion
                  key={item.id}
                  open={open === item.id}
                  icon={<Icon id={item.id} open={open} />}
                >
                  <AccordionHeader
                    onClick={() => handleOpen(item.id)}
                    className="text-lg lg:text-xl"
                  >
                    {item.question}
                  </AccordionHeader>
                  <AccordionBody className="text-md lg:text-lg">
                    {item.answer}
                  </AccordionBody>
                </Accordion>
              ))}
            </>
          ) : (
            <>
              {faqList.slice(0, 10).map((item) => (
                <Accordion
                  key={item.id}
                  open={open === item.id}
                  icon={<Icon id={item.id} open={open} />}
                >
                  <AccordionHeader
                    onClick={() => handleOpen(item.id)}
                    className="text-lg lg:text-xl"
                  >
                    {item.question}
                  </AccordionHeader>
                  <AccordionBody className="text-md lg:text-lg">
                    {item.answer}
                  </AccordionBody>
                </Accordion>
              ))}
            </>
          )}

          <div className="flex items-center justify-center mt-10 text-xl font-semibold">
            <button onClick={handleClick}>{text}</button>
          </div>
        </motion.div>
      </div>
    </>
  );
}

export default Questions;
