import Blog from "./Blog";
import Footer from "./Footer";
import GatedCommunitiesBanner from "./GatedCommunitiesBanner";
import GatedCommunityBooking from "./GatedCommunityBooking";
import Navbar from "./Navbar";
import Process from "./Process";
import ProtectInvestment from "./ProtectInvestment";
import Questions from "./Questions";

function GatedCommunities() {
  return (
    <>
      <Navbar blackTheme={true}/>
      <GatedCommunitiesBanner/>
      <GatedCommunityBooking/>
      <ProtectInvestment/>
      <Process/>
      <Questions/>
      <Blog/>
      <Footer/>
    </>
  );
}

export default GatedCommunities;
