import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import React, { useEffect, useState } from "react";

function valuetext(value) {
  return `${value}`;
}

const Calculator = React.forwardRef((props, ref) => {
  const [selectedState, setSelectedState] = useState("tg");
  const [sliderValue, setSliderValue] = useState(1000);

  const [systemSize, setSystemSize] = useState(1);
  const [spaceRequired, setSpaceRequired] = useState(60);
  const [annualEnergy, setAnnualEnergy] = useState(1440);
  const [annualSavings, setAnnualSavings] = useState(0);


  // eslint-disable-next-line react-hooks/exhaustive-deps
  const keyValuePairOfPrice = {
    1000: 58420,
    2000: 88720,
    3000: 133120,
    4000: 186320,
    5000: 251220,
    6000: 316120,
    7000: 381020,
    8000: 445920,
    9000: 510820,
    10000: 575720,
  };
  const [priceExcludingSubsidy, setPriceExcludingSubsidy] = useState(null);

  const keyValuePairOfSubsidy= {
    1000: 30000,
    2000: 60000,
    3000: 78000,
    4000: 78000,
    5000: 78000,
    6000: 78000,
    7000: 78000,
    8000: 78000,
    9000: 78000,
    10000: 78000,
  };
  const [subsidy, setSubsidy] = useState(null);

  useEffect(() => {
    const calculatedSystemSize = Math.round(sliderValue / 1000);
    const calculatedSpaceRequired = Math.round(calculatedSystemSize * 60);
    const calculatedAnnualEnergy = Math.round(calculatedSystemSize * 1440);
    const calculatedAnnualSavings = Math.round(calculatedSystemSize * 0); 
    setSystemSize((calculatedSystemSize));
    setSpaceRequired(new Intl.NumberFormat('en-IN').format(calculatedSpaceRequired));
    setAnnualEnergy(new Intl.NumberFormat('en-IN').format(calculatedAnnualEnergy));
    setAnnualSavings(new Intl.NumberFormat('en-IN').format(calculatedAnnualSavings));
    setPriceExcludingSubsidy(new Intl.NumberFormat('en-IN').format(keyValuePairOfPrice[sliderValue]));
    setSubsidy(new Intl.NumberFormat().format(keyValuePairOfSubsidy[sliderValue]));
  }, [keyValuePairOfPrice, keyValuePairOfSubsidy, sliderValue]);

  const handleSliderChange = (event, newValue) => {
    setSliderValue(newValue);
  };


  return (
    <div
      ref={ref}
      className="flex flex-col lg:flex-row gap-4 justify-center items-center px-4 md:px-20 lg:px-36 py-12 mb-16 scroll-mt-32"
    >

      <div className="flex flex-col gap-4 p-6">
        <h2 className="text-2xl font-semibold mb-2">Calculate Your Savings</h2>
        <p className="text-sm mb-4">
          Explore the Potential of Solar Energy and Start Saving From Day 1!
        </p>
        <div className="flex flex-col md:flex-row items-center justify-center gap-4">
          <div className="w-full md:w-1/2">
            <label className="font-semibold">Do You Need Solar For</label>
            <select className="w-full px-3 py-2 border border-gray-300 rounded-md">
              <option value="Home">Home</option>
              <option value="commercial">Commercial</option>
            </select>
          </div>
          <div className="w-full md:w-1/2">
            <label className="font-semibold">Select your state</label>
            <select
              value={selectedState}
              onChange={(e) => setSelectedState(e.target.value)}
              className="w-full px-3 py-2 border border-gray-300 rounded-md"
            >
              <option value="tg">Telangana</option>
              <option value="ap">Andhra Pradesh</option>
            </select>
          </div>
        </div>
        <label className="mt-6">Enter monthly electric bill (in ₹)</label>
        <Box sx={{ width: "100%" }} className="px-2">
          <Slider
            aria-label="Electric Bill"
            defaultValue={1000}
            value={sliderValue}
            getAriaValueText={valuetext}
            onChange={handleSliderChange}
            valueLabelDisplay="auto"
            step={1000}
            marks
            min={1000}
            max={10000}
          />
        </Box>
      </div>
      <div className="w-full lg:w-1/2 flex flex-col gap-4 p-6 bg-gray-100 rounded-xl shadow-md">
        <div className="grid grid-cols-2 gap-4">
          <div className="">
            <label htmlFor="systemSize" className="block mb-2">
              System Size
            </label>
            <input
              type="text"
              id="systemSize"
              value={`${systemSize} kW`}
              placeholder="System Size"
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              readOnly
            />
          </div>
          <div className="">
            <label htmlFor="spaceRequired" className="block mb-2">
              Space Required
            </label>
            <input
              type="text"
              id="spaceRequired"
              value={`${spaceRequired} sqft`}
              placeholder="Space Required"
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              readOnly
            />
          </div>
          <div className="">
            <label htmlFor="annualEnergy" className="block mb-2">
              Annual Energy Generated
            </label>
            <input
              type="text"
              id="annualEnergy"
              value={`${annualEnergy} units`}
              placeholder="Annual Energy Generated"
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              readOnly
            />
          </div>
          <div className="">
            <label htmlFor="annualSavings" className="block mb-2">
              Annual Savings
            </label>
            <input
              type="text"
              id="annualSavings"
              value={`₹ ${annualSavings}`}
              placeholder="Annual Savings"
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              readOnly
            />
          </div>
          <div className="">
            <label htmlFor="priceExcludingSubsidy" className="block mb-2">
              Price (Excluding Subsidy)
            </label>
            <input
              type="text"
              id="priceExcludingSubsidy"
              value={`₹ ${priceExcludingSubsidy}`}
              placeholder="Price (Excluding Subsidy)"
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              readOnly
            />
          </div>
          <div className="">
            <label htmlFor="subsidy" className="block mb-2">
              Subsidy
            </label>
            <input
              type="text"
              id="subsidy"
              value={`₹ ${subsidy}`}
              placeholder="Subsidy"
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
              readOnly
            />
          </div>
        </div>
      </div>
    </div>
  );
});

Calculator.displayName = "Calculator";

export default Calculator;
