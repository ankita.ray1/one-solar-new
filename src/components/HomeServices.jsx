import FiveItems from "./FiveItems";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

function HomeServices() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: "-50px 0px",
  });
  const headingVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 1,
      },
    },
  };
  const containerVariants = {
    hidden: {
      opacity: 0,
      y: 50,
    },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 1,
        ease: "easeOut",
      },
    },
  };

  return (
    <>
      <div className="px-4 md:px-20 lg:px-36 my-16">
        <motion.h1
          className="font-inter font-semibold text-2xl md:text-3xl lg:text-4xl text-center lg:text-start"
          variants={headingVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
        >
          Solar Solutions Tailored to Your Home
        </motion.h1>

        <motion.div
          ref={ref}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          variants={containerVariants}
        >
          <FiveItems />
        </motion.div>
      </div>
    </>
  );
}

export default HomeServices;
