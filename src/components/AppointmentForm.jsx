import { useRef } from "react";
import emailjs from "@emailjs/browser";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function AppointmentForm() {
  const form = useRef();
  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm("service_orsh5wt", "template_zx92ng4", form.current, {
        publicKey: "JlE9y5wfe6ecvp6FB",
      })
      .then(
        () => {
          console.log("SUCCESS!");
          toast.success("Your appointment has been scheduled successfully!")
        },
        (error) => {
          console.log("FAILED...", error.text);
          toast.error("Failed to send email. Please try again!")
        }
      );
  };

  return (
    <div className="mx-auto max-w-md lg:max-w-lg">
      <form
        ref={form}
        onSubmit={sendEmail}
        className="bg-white px-7 py-10 rounded-xl shadow-xl hover:shadow-2xl transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105"
      >
        <h2 className="text-md md:text-lg lg:text-xl font-medium mb-4 text-center">
          Unlock the Power of the Sun: Schedule Your Free Solar Consultation
          Today.
        </h2>
        <div className="mb-2">
          <input
            type="text"
            id="name"
            name="name"
            placeholder="Full Name"
            pattern="[A-Za-z\s]+"
            required
            className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="mb-2">
          <input
            type="tel"
            id="mobile"
            name="mobile"
            placeholder="Mobile Number"
            pattern="\d{10}"
            required
            className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="mb-2">
          <input
            type="email"
            id="email"
            name="email"
            placeholder="Email"
            required
            className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
          />
        </div>
        {/* <div className="mb-2">
          <input
            type="text"
            id="bill"
            name="bill"
            placeholder="Monthly Electricity Bill"
            className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
          />
        </div>
        <div className="flex flex-wrap gap-2 mb-2">
          <button className="rounded-full border border-gray-300 px-2 py-1 text-xs">
            &#8377;0-1500
          </button>
          <button className="rounded-full border border-gray-300 px-2 py-1 text-xs">
            &#8377;1500-2500
          </button>
          <button className="rounded-full border border-gray-300 px-2 py-1 text-xs">
            &#8377;2500-4000
          </button>
          <button className="rounded-full border border-gray-300 px-2 py-1 text-xs">
            &#8377;4000-8000
          </button>
          <button className="rounded-full border border-gray-300 px-2 py-1 text-xs">
            More
          </button>
        </div>
        <div className="flex items-center gap-4 mb-2">
          <span className="text-gray-700">Do you own a home?</span>
          <button className="rounded-full border border-gray-300 w-16 py-1 text-sm">
            Yes
          </button>
          <button className="rounded-full border border-gray-300 w-16 py-1 text-sm">
            No
          </button>
        </div> */}
        <div className="mb-2">
          <input
            type="text"
            id="pincode"
            name="pincode"
            placeholder="Zip Code"
            pattern="\d{6}"
            required
            className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
          />
        </div>
        <button
          type="submit"
          className="bg-blue-950 text-white bg-indigo-900 py-3 px-4 w-full rounded-full hover:bg-indigo-800"
        >
          Schedule an appointment
        </button>
        
      </form>
      <ToastContainer />
    </div>
  );
}

export default AppointmentForm;
